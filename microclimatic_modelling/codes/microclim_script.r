#microclimatic modelling 
#20191108

setwd("/home/ddare/Documents/DanieleDaRe/UCL/02_Codes/GitLab_repo_DDR/microclimatic_modelling/codes/")
library(devtools)
install_github('mrke/NicheMapR')
install_github('ilyamaclean/microclima')


require(raster)
require(microclima)
require(NicheMapR)
# Calculate leaf area index
l <- lai(aerial_image[,,3], aerial_image[,,4]) # leaf area
l <- lai_adjust(l, veg_hgt, hgt = 0.05)
x <- leaf_geometry(veg_hgt)  # leaf angle
fr <- canopy(l, x)  # canopy cover
# Calculate ground and canopy albedo
alb <- albedo(aerial_image[,,1], aerial_image[,,2], aerial_image[,,3],  aerial_image[,,4])
albg <- albedo2(alb, fr)
albc <- albedo2(alb, fr, ground = FALSE)
# crop datasets
e <- extent(169400, 169700, 12400, 12700)
l <- crop(if_raster(l, dtm1m), e)
x <- crop(x, e)
fr <- crop(if_raster(fr, dtm1m), e)
albg <- crop(if_raster(albg, dtm1m), e)
albc <- crop(if_raster(albc, dtm1m), e)
dem <- crop(dtm1m, e)
# Run model over 24 hours
tout <- runauto(dem, "27/05/2010", "28/05/2010", hgt = 0.05, 
                l = l, x = x, albg = albg, albc = albc)
# Extract data for 13:00 hrs and convert to raster
temp13 <- if_raster(tout$temps[,,14], dem)
e <- extent(169400, 169700, 12400, 12700)
temp13 <- crop(temp13, e)
mypal <- colorRampPalette(c("darkblue", "blue", "green", 
                            "yellow", "orange", "red"))(255)
par(mfrow=c(1, 1))
plot(temp13, col = mypal, cex.axis = 2, cex.lab = 2, legend.width	= 2, axis.args = list(cex.axis = 2))
