#convert tiff to jpg

for f in *.tiff; do  
	echo "Converting $f"; 
	convert "$f"  "$(basename "$f" .tiff).jpg"; 
done

#conver to pdf
convert *jpg sandbox20190725.pdf


#resize image

convert example.png -resize 200x100! example.png

convert Fig1.TIF -resize  5000x4000! example.png

for f in *.TIF; do  
	echo "Converting $f"; 
	convert "$f" -resize 5000x4000!  "$(basename "$f" .tiff).jpg"; 
done

